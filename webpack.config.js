const path = require('path');
const CleanWebpackPlugin = require(`clean-webpack-plugin`);
module.exports = {
  entry: './src/index.ts',
  output: {
    filename: 'server.js',
    path: path.resolve(__dirname, 'dist')
  },
  plugins: [
  ],
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: [
          'ts-loader'
        ]
      }
    ]
  }
}