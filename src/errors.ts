interface IError {
  path: string,
  msg: string
}

export class ExpressValidationError extends Error {

  public name: string = 'ExpressValidationError';

  constructor(public message: string, public errors: IError[]) {
    super(message);

    Object.setPrototypeOf(this, ExpressValidationError.prototype);
  }
}

export class AuthError extends Error {
  public name: string = 'AuthError';

  constructor(public message: string) {
    super(message);

    Object.setPrototypeOf(this, AuthError.prototype);
  }
}