import { Schema, model, Model, Document } from "mongoose";
const uniqueValidator = require('mongoose-unique-validator');
import * as crypto from 'crypto';

export enum USER_ROLE {
  ADMIN = 1,
  OWNER = 2,
  GUEST = 3
}

export class UserClass {

  constructor() {

  }

  public static async login(username, password): Promise<{}> {
    return new Promise((resolve, reject) => {
      UserModel.findOne({ username }, (error: Error, user: IUserDocument) => {
        if (error) {
          reject(error);
          return;
        }

        if (!user) {
          reject(new Error('Wrong credentials'));
          return;
        }

        if (!user.verifyPassword(password)) {
          reject(new Error('Wrong credentials'));
          return;
        }

        resolve({ id: user.id, username: user.username });
      });
    });
  }

  public static async register(username, email, password): Promise<{}> {
    return new Promise(async (resolve, reject) => {
      const user = new UserModel({ username, email });
      user.setPassword(password);
      try {
        const userDoc = await user.save();
        resolve(userDoc);

      } catch (error) {
        reject(error);
      }
    });
  }

  public static async getUserInfo(uid: string): Promise<{}> {
    return new Promise((resolve, reject) => {
      UserModel.findById(uid, (error: Error, user: IUserDocument) => {
        if (error) {
          reject(error);
          return;
        }

        resolve({ id: user.id, username: user.username, email: user.email });
      });
    });
  }

  public static async getUsersListByParam(param: string): Promise<{id: string, username: string, email: string}[]> {
    const searchStr = new RegExp(param, 'i');
    const users = await UserModel.find()
      .or([{username: searchStr}, {email: searchStr}])
      .exec();

    return users.map((item) => ({id: item.id, username: item.username, email: item.email}));
  }
}

export interface IUserDocument extends Document {
  email: string;
  username: string;
  salt: string;
  hash: string;
  setPassword(password: string);
  verifyPassword(password: string);
}

export interface IUserModel extends Model<IUserDocument> {

}

export const UserSchema: Schema = new Schema({
  username: { type: String, unique: true, required: [true, "required"], maxlength: 255 },
  email: { type: String, unique: true, required: [true, "required"], match: [/\S+@\S+\.\S+/, 'invalid'], maxlength: 255 },
  salt: String,
  hash: String
}, { timestamps: true });

UserSchema.plugin(uniqueValidator);

// UserSchema.methods.setPassword = function(password: string): void {
//   this.salt = crypto.randomBytes(16).toString('hex');
//   this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
// }

UserSchema.method('setPassword', function (password: string): void {
  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
});

UserSchema.methods.verifyPassword = function (password): boolean {
  const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
  return hash === this.hash;
}

export const UserModel: IUserModel = model<IUserDocument>('User', UserSchema);

