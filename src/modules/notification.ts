import { Schema, Document, Model, model } from "mongoose";
import { sendNotification, PushSubscription } from "web-push";
import { IPingerDocument } from "./pinger";
import { IProjectDocument, Project } from "./projects";
const uniqueValidator = require('mongoose-unique-validator');

export class Notification {
  public static async createSubscription(user_id: string, data: PushSubscription): Promise<{}> {
    const pushNotification = { endpoint: data['endpoint'], keys: data['keys'] };
    await SubscriberModel.findOneAndUpdate({ user_id: user_id }, { $push: { subscriptions: pushNotification } }, { upsert: true, runValidators: true });
    await this.sendNotification(user_id, 'You successfully subscribed to push notification!');
    return true;
  }

  public static async sendNotification(user_id: string, body: string): Promise<{}> {
    const subscriber = (await SubscriberModel.findOne({ user_id }));
    if (!subscriber)
      return false;

    // await Promise.all(
    //   subscriber.subscriptions.map(async (subscription): Promise<{}> => {
    //     try {
    //       const push = await sendNotification(subscription, JSON.stringify({
    //         notification: {
    //           title: 'PINGER',
    //           body,
    //           requireInteraction: true,
    //           actions: [
    //             {
    //               action: 'see-more',
    //               title: 'See more'
    //             }
    //           ],
    //           icon: '/assets/images/pingerIcon.png'
    //         }
    //       }));
    //       return push;
    //     } catch (error) {
    //       this.removeSubscription(user_id, subscription);
    //     }
    //   })
    // );

    return true;
  }

  public static async sendDownNotification(user_id: string, pinger: IPingerDocument, errorStatus: string): Promise<{}> {
    const subscriber = (await SubscriberModel.findOne({ user_id }));
    if (!subscriber)
      return false;

    const project = await Project.getProjectByPingerId(pinger.id);

    const payload = {
      notification: {
        title: `${project.name} - ${pinger.name} ${errorStatus} error`,
        body: `Project: ${project.name};  Page: ${pinger.name};  Error code: ${errorStatus}`,
        requireInteraction: true,
        data: {
          url: `/projects/${project.id.substr(-12)}`
        },
        tag: `${project.id.substr(-12)}-${pinger.id.substr(-12)}`,
        renotify: true,
        actions: [
          {
            action: 'see-more',
            title: 'See more'
          }
        ],
        icon: '/assets/images/pingerIcon.png'
      }
    }

    await Promise.all(
      subscriber.subscriptions.map(async (subscription): Promise<{}> => {
        try {
          const push = await sendNotification(subscription, JSON.stringify(payload));
          return push;
        } catch (error) {
          this.removeSubscription(user_id, subscription);
          throw error;
        }
      })
    );

    return true;
  }

  public static async removeSubscription(user_id: string, subscription: PushSubscription): Promise<{}> {
    return await SubscriberModel.findOneAndUpdate({ user_id }, { $pull: { 'subscriptions': { 'endpoint': subscription['endpoint'] } } });
  }
}

const PushNotificationSchema: Schema = new Schema({
  endpoint: { type: String, required: true },
  keys: {
    p256dh: { type: String, required: true },
    auth: { type: String, required: true }
  }
});

export interface ISubscriberDocument extends Document {
  user_id: Schema.Types.ObjectId,
  subscriptions: any[],
  failedNotificationsCount: number
}

export interface ISubscriberModel extends Model<ISubscriberDocument> { }

export const SubscriberSchema: Schema = new Schema({
  user_id: { type: Schema.Types.ObjectId, ref: 'User', required: true, unique: true },
  subscriptions: [PushNotificationSchema],
  failedNotificationsCount: { type: Number, default: 0 }
});

SubscriberSchema.plugin(uniqueValidator);

SubscriberSchema.pre('findOneAndUpdate', async function () {
  const user_id = this.getQuery().user_id;
  const updating = this.getUpdate();

  if (!updating.$push)
    return false;

  const subscription = updating.$push.subscriptions;

  const doc = await SubscriberModel.findOne({ user_id }).elemMatch('subscriptions', { 'endpoint': subscription['endpoint'] });
  if (!!doc)
    throw new Error('ALREADY_EXIST');
});

export const SubscriberModel: ISubscriberModel = model<ISubscriberDocument>('Subscriptions', SubscriberSchema);