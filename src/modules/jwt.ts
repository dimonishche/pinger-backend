import * as fs from 'fs';
import * as JWT from 'jsonwebtoken';
import {join} from 'path';

const PRIVATE_KEY: Buffer = fs.readFileSync(join(__dirname, '../config/private.pem'));
const PUBLIC_KEY: Buffer = fs.readFileSync(join(__dirname, '../config/public.pem'));

export class JWTClass {

  private static readonly EXP_PERIOD: number = 86400;

  constructor() {

  }

  public static async generateJWT(uid: string): Promise<{}> {
    return new Promise((resolve, reject) => {
      JWT.sign(
        {
          uid: uid,
          exp: Math.floor(Date.now() / 1000) + this.EXP_PERIOD
        },
        PRIVATE_KEY,
        {
          algorithm: 'RS256'
        },
        (error: Error, encoded: string) => {
          if (error) {
            reject(error);
            console.log(error)
            return;
          }

          resolve(encoded);
        });
    });
  }

  public static async verifyJWT(jwt: string): Promise<any> {
    return new Promise((resolve, reject) => {
      JWT.verify(jwt, PUBLIC_KEY, (error: Error, decoded) => {
        if (error) {
          reject(error);
          return;
        }

        resolve(decoded);
      });
    });
  }
}

export const verifyJWTMIddleware = async (req, res, next) => {
  const token = req.header('Auth-token');
  if (!token) {
    res.sendStatus(401);
    res.end();
    return;
  }

  try {
    const payload = await JWTClass.verifyJWT(token);
    const newToken = await JWTClass.generateJWT(payload.uid);
    req.user = {uid: payload.uid, token: newToken};
    res.set('Token', newToken);
    next();
  } catch {
    res.sendStatus(401);
    return;
  }
}