import { Document, Model, Schema, model, Types } from 'mongoose';
import { Cron } from '../cron';
import { Project, ProjectModel, IProjectDocument } from './projects';
const uniqueValidator = require('mongoose-unique-validator');

export enum PingerStatus {
  UNDEFINED = 0,
  UP = 1,
  DOWN = 2,
  PAUSED = 3
}

export class Pinger {

  constructor() {

  }

  public static async create(project_id: string, name: string, url: string, user_id: string): Promise<{}> {
    const pinger = await (new PingerModel({ name, url, user_id })).save();
    await Project.setPingerToProject(project_id, pinger._id, user_id);
    // Cron.loadPingers();

    return pinger;
  }

  public static async editPingerData(id: string, uid: string, name: string, url: string): Promise<{}> {
    //use methods find and save, because with update validation works incorrect
    const pinger = await PingerModel.findOne({ _id: id, user_id: uid });
    pinger.name = name;
    pinger.url = url;
    await pinger.save();

    // Cron.loadPingers();
    return pinger;
  }

  public static async delete(project_id: string, id: string, uid: string): Promise<{}> {
    await PingerModel.findOneAndDelete({ _id: id, user_id: uid });
    await Project.removePingerFromProject(project_id, id, uid);

    // Cron.loadPingers();
    return { message: 'Pinger successfully deleted.' };
  }

  public static async deleteProjectPingers(pingers_id: Schema.Types.ObjectId[], uid: string): Promise<any> {
    await PingerModel.deleteMany({ user_id: uid }).in('_id', pingers_id).exec();
    // Cron.loadPingers();
    return true;
  }

  public static async getUserPingers(uid: string): Promise<{}> {
    const pingers = await PingerModel.find({ user_id: uid });
    return pingers.map(item => ({
      name: item.name,
      url: item.url,
      id: item.id,
      status: item.status,
      requests_count: item.requests_count,
      up_count: item.up_count,
      down_count: item.down_count,
      up_from: item.up_from,
      avg_time: item.avg_time
    }));
  }

  public static async getPingersByProject(project_id: string, uid: string) {
    const result = await ProjectModel.findOne()
      .or([{ _id: project_id, user_id: uid }, { accepted_users: uid, _id: project_id, }])
      .select('pingers')
      .populate({ path: 'pingers', select: 'name url _id status requests_count up_count down_count up_from avg_time' })
      .exec();

    const userRole = Project.getDocUserRole(result, uid);
    return result.pingers.map(item => ({ ...item['toJSON'](), userRole }));
  }

  public static async getPingerById(id: string): Promise<IPingerDocument> {
    return await PingerModel.findById(id);
  }

  public static async getPingerInfoByShortId(project: IProjectDocument, pinger_short_id: string, uid: string): Promise<IPingerDocument> {
    const pinger = (await PingerModel.findOne({ user_id: project.user_id, $where: `this._id.str.match(/${pinger_short_id}$/)` })).toJSON();

    pinger.down_list.sort((prevVal, currentVal) => (prevVal.time_from < currentVal.time_from) ? 1 : -1);
    return pinger;
  }

  public static async getAll(): Promise<IPingerDocument[]> {
    return await PingerModel.find({ status: { $ne: PingerStatus.PAUSED } });
  }

  public static async pausePinger(id: string, uid: string): Promise<IPingerDocument> {
    const pinger: IPingerDocument = await PingerModel.findOneAndUpdate({ _id: id, user_id: uid }, { status: PingerStatus.PAUSED });
    // Cron.loadPingers();
    return pinger;
  }

  public static async startPinger(id: string, uid: string): Promise<IPingerDocument> {
    const pinger: IPingerDocument = await PingerModel.findOneAndUpdate({ _id: id, user_id: uid }, { status: PingerStatus.UNDEFINED });
    // Cron.loadPingers();
    return pinger;
  }

}


export interface IDownItemDocument extends Document {
  error: String;
  time_from: Date;
  time_to: Date;
  message: String;
  isNotified: Boolean;
}

export interface IDownItemModel extends Model<IDownItemDocument> {

}

export const DownItemSchema: Schema = new Schema({
  error: { type: String, required: [true, "required"], maxlength: 255 },
  time_from: { type: Date, required: [true, "required"], default: Date.now },
  time_to: { type: Date, required: [true, "required"], default: Date.now },
  message: { type: String, maxlength: 1000 },
  isNotified: { type: Boolean, default: false }
});

export const DownItemModel: IDownItemModel = model<IDownItemDocument>('DownItem', DownItemSchema);

export interface IPingerDocument extends Document {
  name: String;
  url: String;
  user_id: String;
  status: Number;
  requests_count: Number;
  up_count: Number;
  down_count: Number;
  up_from: Date;
  avg_time: Number;
  down_list: IDownItemDocument[];
}

export interface IPingerModel extends Model<IPingerDocument> {

}

export const PingerSchema: Schema = new Schema({
  name: { type: String, required: [true, "required"], maxlength: 255 },
  url: { type: String, required: [true, "required"], maxlength: 1000 },
  user_id: String,
  status: { type: Number, required: [true, "required"], default: PingerStatus.UNDEFINED },
  requests_count: { type: Number, required: [true, "required"], default: 0 },
  up_count: { type: Number, required: [true, "required"], default: 0 },
  down_count: { type: Number, required: [true, "required"], default: 0 },
  up_from: { type: Date, default: null },
  avg_time: { type: Number, default: null },
  down_list: { type: [DownItemSchema], default: [] }
}, { timestamps: true });

PingerSchema.index({ name: 1, user_id: 1 }, { unique: true });
PingerSchema.index({ url: 1, user_id: 1 }, { unique: true });

PingerSchema.plugin(uniqueValidator);


export const PingerModel: IPingerModel = model<IPingerDocument>('Pingers', PingerSchema);