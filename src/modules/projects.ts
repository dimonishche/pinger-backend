import { Document, Model, Schema, model } from "mongoose";
import { PingerStatus, Pinger, IPingerDocument } from "./pinger";
import { USER_ROLE } from "./user";
const uniqueValidator = require('mongoose-unique-validator');

export class Project {

  public static async create(name, origin, user_id): Promise<IProjectDocument> {
    const project = await (new ProjectModel({ name, origin, user_id, accepted_users: [] })).save();
    return project;
  }

  public static async getProjectsByOwner(uid: string): Promise<IProjectDocument[]> {
    return await ProjectModel.find({ user_id: uid }).populate({
      path: 'pingers',
      select: 'status avg_time'
    }).exec();
  }

  public static async getProjectsByGuest(guest_id: string): Promise<IProjectDocument[]> {
    return await ProjectModel.find().where('accepted_users', guest_id).populate({
      path: 'pingers',
      select: 'status avg_time'
    }).exec();
  }

  public static async prettyfyProjects(projects: IProjectDocument[]) {
    return await Promise.all(
      projects.map(async (project): Promise<{}> => {
        const active = project.pingers.filter(item => item['status'] === PingerStatus.UP || item['status'].UNDEFINED).length;
        const down = project.pingers.filter(item => item['status'] === PingerStatus.DOWN).length;
        const paused = project.pingers.filter(item => item['status'] === PingerStatus.PAUSED).length;
        const avgTime = project.pingers
          .map((item): number => item['avg_time'])
          .reduce((prevValue, currentValue, index): number => {
            return (index * prevValue + currentValue) / (index + 1);
          }, 0);

        return { id: project._id, name: project.name, origin: project.origin, active, down, paused, count: project.pingers.length, avgTime: avgTime.toFixed(2) };
      })
    );
  }

  //get project by last 12 symbols of id
  public static async getProjectInfoByShortId(short_id: string, uid: string): Promise<{ role: USER_ROLE, project: IProjectDocument }> {
    const project = await ProjectModel.findOne()
      .or([{ user_id: uid, $where: `this._id.str.match(/${short_id}$/)` }, { accepted_users: uid, $where: `this._id.str.match(/${short_id}$/)` }])
      .populate({path: 'accepted_users', select: 'email username _id'})
      .populate('pingers')
      .exec();

    if (project['user_id'] === uid)
      return { role: USER_ROLE.OWNER, project };

    return { role: USER_ROLE.GUEST, project }
  }

  public static async getProjectById(project_id: string, uid: string): Promise<{ role: USER_ROLE, project: IProjectDocument }> {
    const project = await ProjectModel.findOne()
      .or([{ _id: project_id, user_id: uid }, { accepted_users: uid, _id: project_id }])
      .exec();

    const role = this.getDocUserRole(project, uid);

    return { role, project };
  }

  public static async getProjectByPingerId(pinger_id: string): Promise<IProjectDocument> {
    return await ProjectModel.findOne().elemMatch('pingers', { $eq: pinger_id });
  }

  public static async updateProject(id, name, origin, uid): Promise<IProjectDocument> {
    const project = await ProjectModel.findOne({ _id: id, user_id: uid });
    project.name = name;
    project.origin = origin;
    await project.save();

    return project;
  }

  public static async removeProject(id, uid): Promise<{}> {
    const removedProject = await ProjectModel.findOneAndDelete({ _id: id, user_id: uid }).exec();
    await Pinger.deleteProjectPingers(removedProject.pingers, uid);

    return { message: 'Project successfully deleted.' };
  }

  public static async addUserToProject(guest_id, project_id, uid): Promise<IProjectDocument> {
    return await ProjectModel.findOneAndUpdate({ _id: project_id, user_id: uid }, { $push: { accepted_users: guest_id } });
  }

  public static async removeUserFromProject(guest_id, project_id, uid): Promise<{}> {
    return await ProjectModel.findOneAndUpdate({ _id: project_id, user_id: uid }, { $pull: { accepted_users: guest_id } });
  }

  public static async setPingerToProject(project_id: string, pinger_id: string, uid: string): Promise<boolean> {
    await ProjectModel.findOneAndUpdate(
      { _id: project_id, user_id: uid },
      { $push: { pingers: pinger_id } }
    );

    return true;
  }

  public static async removePingerFromProject(project_id: string, pinger_id: string, uid: string): Promise<boolean> {
    await ProjectModel.findOneAndUpdate(
      { _id: project_id, user_id: uid },
      { $pull: { pingers: pinger_id } }
    );

    return true;
  }

  public static getDocUserRole(doc: IProjectDocument | IPingerDocument, uid: string): USER_ROLE {
    if (doc['user_id'] === uid)
      return USER_ROLE.OWNER;

    return USER_ROLE.GUEST;
  }
}

export interface IProjectDocument extends Document {
  name: String;
  origin: String;
  user_id: String;
  accepted_users: Schema.Types.ObjectId[];
  pingers: Schema.Types.ObjectId[];
}

export interface IProjectModel extends Model<IProjectDocument> {

}

export const ProjectSchema: Schema = new Schema({
  name: { type: String, requried: [true, "required"], maxlength: 255 },
  origin: { type: String, required: [true, "required"], maxlength: 255 },
  user_id: { type: Schema.Types.ObjectId, ref: 'User' },
  accepted_users: [{ type: Schema.Types.ObjectId, ref: 'User' }],
  pingers: [{ type: Schema.Types.ObjectId, ref: 'Pingers' }]
});

ProjectSchema.index({ name: 1, user_id: 1 }, { unique: true });
ProjectSchema.index({ origin: 1, user_id: 1 }, { unique: true });

ProjectSchema.plugin(uniqueValidator);

ProjectSchema.pre('findOneAndUpdate', async function () {
  const user_id = this.getQuery().user_id;
  const _id = this.getQuery()._id;
  const updating = this.getUpdate();

  if (!updating.$push)
    return false;

  if (!updating.$push.accepted_users)
    return false;

  const accepted_users = updating.$push.accepted_users;

  const doc = await ProjectModel.findOne({ _id, user_id }).where('accepted_users', accepted_users);

  if (!!doc)
    throw new Error('ALREADY_EXIST');
});

export const ProjectModel: IProjectModel = model<IProjectDocument>('Projects', ProjectSchema);