import request from 'request';
import { PingerStatus, DownItemModel, Pinger, IDownItemDocument } from './pinger';
import { Notification } from './notification';

export class UrlChecker {

  public static async checkPingers() {
    const pingers = <any[]>await Pinger.getAll();
    UrlChecker.check(pingers);
  }

  public static check(pingersArray, index = 0): void {
    if (!pingersArray[index])
      return;

    if (pingersArray[index].status === PingerStatus.PAUSED)
      return this.check(pingersArray, index + 1);

    request(pingersArray[index].url, { time: true }, (error, res, body) => {
      this.check(pingersArray, index + 1);
      this.processResponse(pingersArray[index], res, body, error);
    });
  }

  private static async processResponse(pingerInfo, res, body, error) {
    try {
      const pingerDoc = await Pinger.getPingerById(pingerInfo.id);
      const status = res ? res.statusCode : 408;

      if (error) {
        await this.failedResponse(pingerDoc, res ? res.timingPhases.total : null, status, error.message);
        return;
      }

      if (status < 400) {
        await this.successResponse(pingerDoc, res.timingPhases.total);
        return;
      }

      this.failedResponse(pingerDoc, res ? res.timingPhases.total : null, status, !!body ? body : 'undefined');
    } catch (e) {
      console.error(e);
    }

  }

  private static async successResponse(pingerInfo, time): Promise<any> {
    const avgTime = this.getAvgTime(pingerInfo.avg_time, pingerInfo.requests_count, time);
    const newData = {
      status: PingerStatus.UP,
      requests_count: ++pingerInfo.requests_count,
      up_count: ++pingerInfo.up_count,
      up_from: (pingerInfo.status === PingerStatus.DOWN || !pingerInfo.up_from) ? new Date() : pingerInfo.up_from,
      avg_time: avgTime
    }

    pingerInfo.set(newData);
    return await pingerInfo.save();
  }

  private static async failedResponse(pingerInfo, time, status, message: string): Promise<any> {

    const avgTime = !!time ? this.getAvgTime(pingerInfo.avg_time, pingerInfo.requests_count, time) : pingerInfo.avg_time;

    const newData = {
      status: PingerStatus.DOWN,
      requests_count: ++pingerInfo.requests_count,
      down_count: ++pingerInfo.down_count,
      avg_time: avgTime
    }

    const lastDownItem: IDownItemDocument = !!pingerInfo.down_list.length ? pingerInfo.down_list[pingerInfo.down_list.length - 1] : undefined;

    if (pingerInfo.status === PingerStatus.DOWN && lastDownItem && lastDownItem.error === status.toString()) {
      lastDownItem.set({ time_to: new Date() });
      if (!lastDownItem.isNotified)
        try {
          await Notification.sendDownNotification(pingerInfo['user_id'], pingerInfo, status);
          lastDownItem.set({ isNotified: true });
        } finally { }
    } else {
      const text = message.length < 1000 ? message : '';
      const downItem = new DownItemModel({ error: status, time_from: new Date(), time_to: new Date(), message: text, isNotified: false });

      try {
        await Notification.sendDownNotification(pingerInfo['user_id'], pingerInfo, status);
        downItem.set({ isNotified: true });
      } finally {
        pingerInfo.down_list.push(downItem);
      }
    }

    pingerInfo.set(newData);
    return await pingerInfo.save();
  }

  private static getAvgTime(prevAvgTime, count, time): number {
    let avgTime = prevAvgTime;
    if (avgTime === null) {
      return time;
    }
    return (avgTime * count + time) / (count + 1);
  }
}