import { connect, connection, ConnectionBase, set } from "mongoose";

export class DB {
  private state: ConnectionBase['readyState'] = connection.readyState;

  constructor() {
    set('useFindAndModify', false);
    set('useCreateIndex', true);
    connect('mongodb://pinger-admin:dwwfiaR2di9YBgq@ds024748.mlab.com:24748/pinger', { useNewUrlParser: true });
  }

  public dbState(): ConnectionBase['readyState'] {
    return this.state;
  }

  public async onConnected(): Promise<any> {
    return new Promise((resolve, reject) => {
      if (this.state === 1) {
        resolve(true);
        return;
      }

      connection.on('connected', () => resolve(true));
      connection.on('disconnected', () => reject(new Error('Cannot connect to DB!')));
    });
  }

  private listenDbState(): void {
    connection.on('connected', () => this.state = connection.readyState);
    connection.on('disconnected', () => this.state = connection.readyState);
  }
}