import express from 'express';
import bodyParser from 'body-parser';
import { DB } from './db';
import { Cron } from './cron';
import { router as pingerRouter } from './controllers/pinger';
import { router as userRouter } from './controllers/user';
import { router as projectRouter } from './controllers/projects';
import { router as notificationRouter } from './controllers/notification';
import * as webpush from 'web-push';
import request from 'request';

const cors = require('cors');

export const app = express();

webpush.setVapidDetails('mailto:dimon.bc8@gmail.com', process.env.PUBLIC_VAPID , process.env.PRIVATE_VAPID);

Cron.start();

app.use(cors());
app.options('*', cors({
  origin: '*'
}));

app.get('*.*', express.static(__dirname));

app.use(bodyParser.json());

app.use('*', async (req, res, next) => {
  const db = new DB();

  try {
    await db.onConnected();
    next();
    return;
  } catch (error) {
    res.status(500).send('Cannot connect to DB.');
    return;
  }
});

app.use('/user', userRouter);
app.use('/pinger', pingerRouter);
app.use('/project', projectRouter);
app.use('/notification', notificationRouter);

app.get('/', (req, res) => {
  // res.sendFile(join(join(process.cwd()), 'index.html'))
  res.send('SUCCESS');
});

app.get('/test-500', (req, res) => {
  res.sendStatus(200);
});

process.on("SIGTERM", () => {
  request('https://pinger-backend.herokuapp.com/');
});

setInterval(() => {
  request('https://pinger-backend.herokuapp.com/');
}, 20 * 60 * 1000);