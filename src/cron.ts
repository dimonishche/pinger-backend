import { CronJob } from 'cron';
import { Pinger } from './modules/pinger';
import { DB } from './db';
import { UrlChecker } from './modules/urlChecker';
import * as webpush from 'web-push';

webpush.setVapidDetails('mailto:dimon.bc8@gmail.com', process.env.PUBLIC_VAPID , process.env.PRIVATE_VAPID);

export class Cron {

  private static job: CronJob;
  private static pingers: any[] = [];

  constructor() {

  }

  public static async start() {
    const db = new DB();

    if (this.job)
      this.job.stop();

    try {
      await db.onConnected();
      await this.createJob();
      this.job.start();
    } catch (error) {
      console.error(error);
    }
  }

  public static stop(): void {
    this.job.stop();
  }

  public static restart(): void {
    this.job.stop();
    this.job.start();
  }

  // public static async loadPingers() {
  //   this.pingers = <any[]>await Pinger.getAll();
  // }

  private static async createJob(): Promise<CronJob> {
    return new Promise<CronJob>((resolve, reject) => {
      this.job = new CronJob('* * * * *', UrlChecker.checkPingers);
      resolve(this.job);
    });
  }
}

// Cron.start();