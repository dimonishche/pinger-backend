import { Router } from 'express';
import { verifyJWTMIddleware } from '../modules/jwt';
import { Pinger } from '../modules/pinger';
const {
  check,
  validationResult
} = require('express-validator/check');
import { asyncMiddleware } from '../middlewares';
import { ExpressValidationError } from '../errors';
import { Project } from '../modules/projects';

export const router = Router();

router.use('*', verifyJWTMIddleware);

const pingerCreateValidation = [
  check('name').trim().exists().withMessage('required').isLength({ min: 1 }).withMessage('required').isLength({ max: 255 }).withMessage('maxlength'),
  check('url').trim().exists().withMessage('required').isLength({ min: 1 }).withMessage('required').isLength({ max: 254 }).withMessage('maxlength')
];

router.post('/create', pingerCreateValidation, asyncMiddleware(async (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    throw new ExpressValidationError('',
      errors.array({
        onlyFirstError: true
      })
    );
  }

  const { project_id, name, url } = req.body;
  const pinger = await Pinger.create(project_id, name, url, req['user'].uid);
  res.send({ result: 'success', data: pinger });
})
);

router.put('/:id', asyncMiddleware(async (req, res, next) => {
  const id = req.params.id;
  const { name, url } = req.body;

  const pinger = await Pinger.editPingerData(id, req['user']['uid'], name, url);
  res.send({ result: 'success', data: pinger });
}));

router.delete('/:project_id/:pinger_id', asyncMiddleware(async (req, res, next) => {
  const { pinger_id, project_id } = req.params;

  if (!pinger_id)
    throw new Error('PINGER_NOT_FOUND');
  if (!project_id)
    throw new Error('PROJECT_NOT_FOUND');

  const result = await Pinger.delete(project_id, pinger_id, req['user']['uid']);
  res.send({ result: 'success', data: result });
}));

router.get('/list', asyncMiddleware(async (req, res) => {
  const pingers = await Pinger.getUserPingers(req['user']['uid']);
  res.status(200).send({ result: 'success', data: pingers });
}));

router.get('/list/:project_id', asyncMiddleware(async (req, res) => {
  const result = await Pinger.getPingersByProject(req.params['project_id'], req['user']['uid']);
  res.status(200).send({ result: 'success', data: result });
}));

router.get('/:project_short_id/:pinger_short_id', asyncMiddleware(async (req, res) => {
  const project = await Project.getProjectInfoByShortId(req.params['project_short_id'], req['user']['uid']);
  const pinger = await Pinger.getPingerInfoByShortId(project.project, req.params['pinger_short_id'], req['user']['uid']);
  res.status(200).send({ result: 'success', data: {project, pinger} });
}));

router.post('/pause', asyncMiddleware(async (req, res) => {
  const pinger = await Pinger.pausePinger(req.body['pinger_id'], req['user']['uid']);
  res.status(200).send({ result: 'success' });
}));

router.post('/start', asyncMiddleware(async (req, res) => {
  const pinger = await Pinger.startPinger(req.body['pinger_id'], req['user']['uid']);
  res.status(200).send({ result: 'success' });
}));