import { Router } from 'express';
import { UserClass, IUserDocument } from '../modules/user';
import { JWTClass, verifyJWTMIddleware } from '../modules/jwt';
import { asyncMiddleware } from '../middlewares';
import { AuthError } from '../errors';

export const router = Router();

router.post('/register', asyncMiddleware(async (req, res, next) => {
  await UserClass.register(req.body.username, req.body.email, req.body.password);
  res.status(200).send({ result: 'success' });
}));

router.post('/login', asyncMiddleware(async (req, res, next) => {
  try {
    const userDoc = await UserClass.login(req.body.username, req.body.password);
    const token = <string>await JWTClass.generateJWT(userDoc['id']);
    res.status(200).send(
      {
        result: 'success',
        data: { username: userDoc['username'], token: token }
      }
    );
  } catch(error) {
    throw new AuthError('LOGIN_ERROR');
  }
}));

router.get('/getInfo', verifyJWTMIddleware, asyncMiddleware(async (req, res) => {
  const user = <IUserDocument>await UserClass.getUserInfo(req['user']['uid']);

  res.status(200).send(
    {
      result: 'success',
      data: { id: req['user']['uid'], username: user.username, email: user.email }
    }
  );
}));

router.post('/verifyAuth', asyncMiddleware(async (req, res) => {
  const token = req.body.token;

  if (!token)
    throw new AuthError('WRONG_JWT');

  try {
    const payload = await JWTClass.verifyJWT(token);
    const newToken = await JWTClass.generateJWT(payload.uid);
    res.send({ result: 'success', token: newToken });
  } catch (error) {
    throw new AuthError('GENERATE_JWT_ERROR');
  }
}));

router.post('/search', verifyJWTMIddleware,  asyncMiddleware(async (req, res) => {
  const param = req.body.param;

  let result = await UserClass.getUsersListByParam(param);

  res.send({result});
}));