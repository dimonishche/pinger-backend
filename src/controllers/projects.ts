import { Router } from 'express';
import { verifyJWTMIddleware } from '../modules/jwt';
import { Project } from '../modules/projects';
import { asyncMiddleware } from '../middlewares';
const {
  check,
  validationResult
} = require('express-validator/check');
import { ExpressValidationError } from '../errors';

export const router = Router();

router.use('*', verifyJWTMIddleware);

const projectCreateValidation = [
  check('name').trim().exists().withMessage('required').isLength({ min: 1 }).withMessage('required').isLength({ max: 255 }).withMessage('maxlength'),
  check('origin').trim().exists().withMessage('required').isLength({ min: 1 }).withMessage('required').isLength({ max: 254 }).withMessage('maxlength')
];

router.post('/create', projectCreateValidation, asyncMiddleware(async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    throw new ExpressValidationError('',
      errors.array({
        onlyFirstError: true
      })
    );
  }

  const { name, origin } = req.body;
  const project = await Project.create(name, origin, req['user'].uid);
  res.send({ result: 'success', data: project });
}));

router.get('/list', asyncMiddleware(async (req, res) => {
  const projects = await Project.getProjectsByOwner(req.user.uid);
  const prettyProjects = await Project.prettyfyProjects(projects);
  res.send({ result: 'success', data: prettyProjects });
}));

router.get('/list-by-guest', asyncMiddleware(async (req, res) => {
  const projects = await Project.getProjectsByGuest(req.user.uid);
  const prettyProjects = await Project.prettyfyProjects(projects);
  res.send({ result: 'success', data: prettyProjects });
}));

router.get('/:short_id', asyncMiddleware(async (req, res) => {
  const short_id = req.params.short_id;
  const project = await Project.getProjectInfoByShortId(short_id, req.user.uid);
  res.send({ result: 'success', data: project });
}));

router.delete('/:project_id', asyncMiddleware(async (req, res) => {
  const project_id = req.params.project_id;

  await Project.removeProject(project_id, req.user.uid);
  res.send({ result: 'success' });
}));

router.put('/:project_id', asyncMiddleware(async (req, res) => {
  const project_id = req.params.project_id;
  const { name, origin } = req.body;

  await Project.updateProject(project_id, name, origin, req.user.uid);
  res.send({ result: 'success' });
}));

router.post('/invite/:project_id', asyncMiddleware(async (req, res) => {
  const project_id = req.params.project_id;
  const { guest_id } = req.body;

  try {
    await Project.addUserToProject(guest_id, project_id, req.user.uid);
    res.send({ result: 'success' });
  } catch (error) {
    if (error.message === 'ALREADY_EXIST')
      res.status(400).send({ result: 'Already exist' });
  }
}));

router.post('/exclude-user/:project_id', asyncMiddleware(async (req, res) => {
  const project_id = req.params.project_id;
  const { guest_id } = req.body;

  await Project.removeUserFromProject(guest_id, project_id, req.user.uid);
  res.send({ result: 'success' });
}));