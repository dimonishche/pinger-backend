import { Router } from 'express';
import { verifyJWTMIddleware } from '../modules/jwt';
import { asyncMiddleware } from '../middlewares';
import { Notification } from '../modules/notification';
import { ExpressValidationError } from '../errors';

export const router = Router();

router.use('*', verifyJWTMIddleware);

router.post('/subscribe', asyncMiddleware(async (req, res) => {
  if (!req.body.subscription)
    throw new ExpressValidationError('', [{ path: 'subscription', msg: 'required' }]);
  try {
    await Notification.createSubscription(req.user.uid, req.body.subscription);
  } catch (error) {
    if (error.message === 'ALREADY_EXIST')
      res.send({ result: 'Already registered' });
      return;
  }
  res.send({ result: 'success' });
}));