export const asyncMiddleware = fn => (req, res, next) => {
  Promise.resolve(fn(req, res, next))
    .catch(error => {
      console.error(error);
      handlingError(error, res);
    });
}

const handlingError = (error: Error, res) => {
  switch (error.name) {
    case "ValidationError":
      res.status(400).send({ errors: handleMongooseValidationErrors(error['errors']) });
      break;
    case "ExpressValidationError":
      res.status(400).send({ errors: error['errors'] });
      break;
    case "AuthError":
      res.status(401).send({ message: error.message });
      break;
    default:
      res.status(500).send({ message: error.message })
  }

}

const handleMongooseValidationErrors = (errors) => {
  let errorsArray = [];
  for (let key in errors) {
    errorsArray.push({ param: key, msg: errors[key].kind });
  }

  return errorsArray;
}